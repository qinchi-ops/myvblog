package token

import "context"

type Service interface {
	// 令牌颁发
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)

	// 令牌撤销
	RevolkToken(context.Context, *RevolkTokenRequest) (*Token, error)

	// 令牌校验, 校验令牌合法性
	ValidateToken(context.Context, *ValidateTokenRequest) (*Token, error)
}

type IssueTokenRequest struct {
	Username string
	Password string
	// 记住我
	IsMember bool
}

type RevolkTokenRequest struct {
	// 你要撤销的令牌
	// AccessToken, RefreshToken构成了一对 username/password
	AccessToken string
	// 你需要知道正确的刷新Token
	RefreshToken string
}

type ValidateTokenRequest struct {
	// 你要撤销的令牌
	// AccessToken, RefreshToken构成了一对 username/password
	AccessToken string
}
