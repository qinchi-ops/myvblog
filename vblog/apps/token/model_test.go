package token_test

import (
	"testing"

	"gitlab.com/go-course-project/go15/vblog/apps/token"
)

func TestTokenString(t *testing.T) {
	tk := token.Token{
		UserId: "admin",
		Role:   "admin",
	}

	// 失败, 退出测试
	// t.Fatal()
	// 打印数据
	t.Log(tk.String())
}
